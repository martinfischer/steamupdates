#! /bin/bash
cd ~/pyvenvs/steamupdates
source bin/activate

cd ~/workspace/steamupdates

# virtualenv is now active, which means your PATH has been modified and python
# can be called like this.

python steamupdates.py

deactivate

# a sample crontab entry:
# to check for jobs by the loged in user: crontab -l
# to edit/create the jobs for the loged in user: crontab -e
# entry: 45 7,15 * * * /home/martin/workspace/steamupdates/steamupdates/steamupdates.sh
