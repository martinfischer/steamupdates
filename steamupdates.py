"""
steamupdates.py

Sucht nach neuen Updates für TF2 und CSGO
"""
import os
import requests
from time import sleep
from datetime import datetime
from bs4 import BeautifulSoup


def write_log(message):
    """
    Schreibt ein Logfile
    """
    now = datetime.now()
    timestamp = now.strftime("%Y-%m-%d %H:%M")
    programm_filename = os.path.basename(__file__)
    message = "{:20}-->  {}  -->  {}\n".format(programm_filename, timestamp, message)
    logfile = os.path.expanduser("~/.applog")
    with open(logfile, "a") as f:
        f.write(message)


def send_message(title, message, sound, priority):
    """
    Sendet eine Pushover Notifikation an mein Tab
    """
    APP_TOKEN = "APP_TOKEN"
    USER_KEY = "USER_KEY"
    payload = {
        "token": APP_TOKEN,
        "user": USER_KEY,
        "title": title,
        "message": message,
        "sound": sound,
        "priority": priority
    }
    try:
        r = requests.post("https://api.pushover.net/1/messages.json",
                          data=payload)
        return r
    except:
        return False


def read_update_dates(update_dates_file):
    """
    List vorhandene Update Daten aus einer Datei
    """
    with open(update_dates_file) as f:
        update_dates = f.read().splitlines()
    return update_dates


def write_update_date(update_dates_file, update_date):
    """
    Schreibt neue Update Daten in ein File
    """
    with open(update_dates_file, "a") as f:
        f.write("{}\n".format(update_date))


def get_latest_update(url, game):
    """
    Holt das letze Update Datum für das jewilige Spiel
    """
    try:
        r = requests.get(url)
        html = r.text
        soup = BeautifulSoup(html)
        if game == "tf2":
            latest_update = soup.h2.get_text()
        elif game == "csgo":
            latest_update_raw = soup.find_all("p",
                                              class_="post_date")[0].get_text()
            latest_update = latest_update_raw.split("  ")[0]
        return (latest_update,)
    except Exception as error:
        return ("Error", error)


def check_update(game, url, update_dates_file, sound, priority):
    """
    Die Hauptfunktion, die alles einleitet
    """
    now = datetime.now()
    timestamp = now.strftime("%Y-%m-%d %H:%M")
    latest_update_tuple = get_latest_update(url, game)
    latest_update = latest_update_tuple[0]
    heavy_load = ("The Steam Store is experiencing some heavy load right now."
                  "  Please try again later.")
    if latest_update == "Error":
        result = send_message("Steamupdates Error", "Error: {}".format(
                                        latest_update_tuple[1]), "updown", "0")
        write_log("{} Error: {} Pushover response: {}".format(
                              game, latest_update_tuple[1], result))
    elif latest_update == heavy_load:
        result = send_message("Steamupdates Heavy Load",
                              "Error: Heavy Load",
                              "updown",
                              "0")
        write_log("{} {} Pushover response: {}".format(game, heavy_load, result))
    else:
        if latest_update in read_update_dates(update_dates_file):
            write_log("No new {} update found".format(game))
        else:
            title = "New {} update".format(game)
            message = "New {} update is out! Check: {}".format(game, url)
            result = send_message(title, message, sound, priority)
            if result:
                write_log(("New {} update found! "
                          "Pushover response: {}").format(game, str(result)))
            else:
                write_log(("New {} update found! "
                          "Pushover connection error").format(game))
            write_update_date(update_dates_file, latest_update)


if __name__ == "__main__":
    tf2_url = ("http://www.steampowered.com/platform/"
               "update_history/index.php?id=440")
    tf2_update_dates_file = "update_dates_tf2.txt"
    check_update("tf2", tf2_url, tf2_update_dates_file, "alien", "0")

    sleep(1)

    csgo_url = "http://blog.counter-strike.net/index.php/category/updates/"
    csgo_update_dates_file = "update_dates_csgo.txt"
    check_update("csgo", csgo_url, csgo_update_dates_file, "climb", "0")
